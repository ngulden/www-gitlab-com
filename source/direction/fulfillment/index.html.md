---
layout: markdown_page
title: Product Vision - Fulfillment
---

## On this page
{:.no_toc}

- TOC
{:toc}

_"When technology delivers basic needs, user experience dominates."_  - Donald Norman

This is the product vision for GitLab's Fulfillment group. Below are the categories Fulfillment currently owns and improves.

If you'd like to discuss this vision directly with the Product Manager for Fulfillment,
feel free to reach out to Luca Williams via [e-mail](mailto:luca@gitlab.com),
[Twitter](https://twitter.com/tipyn2903), or by [scheduling a video call](https://calendly.com/tipyn).

| Category | Description |
| ------ |  ------ |
| [Account Management](/direction/fulfillment/account-management) | How our users purchase and renew GitLab Plans and add-ons, and how they manage their account information and subscriptions. |

## ⛵️ Overview

The overall vision for Fulfillment is to provide GitLab’s customers with invisible and seamless purchase, renewal, upgrade and account management experiences with GitLab. Additionally, Fulfillment strives to provide GitLab team members with great internal tools so that they may efficiently support and serve our users.

Billing, account management and license keys can be confusing, boring and time-consuming. We want these things to be easy and frictionless so you can focus on enjoying our product. In addition to our customers, we also prioritise the experience of GitLab Team Members (and Resellers). In order for our GitLab Team Members and Resellers to provide a great service to our customers, the administration and 'behind-the-scenes' user experience of our billing and licensing services are held to the same standard as the rest of our product.

Due to the fact that the Fulfillment group is responsible for collecting and storing personal and billing information, we also need to ensure that GitLab's services meet all compliance, security and privacy requirements.

### ⭐️ North Stars

1. [Make renewals, upgrades and true-ups easy for customers and GitLab team members](https://gitlab.com/groups/gitlab-org/-/epics/1872)
2. [Make it easy and delightful for new customers to buy GitLab](https://gitlab.com/groups/gitlab-org/-/epics/1873)

Please click into the epics to see what's next and why.
 
## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Fulfillment can be viewed [on the issue board](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Afulfillment), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!
