--- 
layout: markdown_page
title: "Investor Relations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Details TBA

## Performance Indicator

### Enterprise Value to Sales
Enterprise Value to Sales compares the enterprise value (EV) of a company to its annual sales. The target for this PI is to be great than or equal to the 75th percentile.

Enterprise Value to Sales = Enterprise Value/Annual Sales

Enterprise Value = Market Capitalization + Debt - Cash and Cash Equivalents
